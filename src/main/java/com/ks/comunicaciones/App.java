package com.ks.comunicaciones;

import com.ks.configuracion;

public class App {
    public static void main(String[] args) {
        if (args.length >= 3){
            clienteTCP cliente = new clienteTCP();
            cliente.setIP(args[0]);
            cliente.setPuerto( Integer.parseInt(args[1]));
            if (configuracion.getRuta().contains("\\")){
                cliente.setFile(configuracion.getRuta() + "transaction\\" + args[2]);
            } else {
                cliente.setFile(configuracion.getRuta() + "transaction/" + args[2]);
            }
            cliente.openFile();
            cliente.conectar();
        }
    }
}
