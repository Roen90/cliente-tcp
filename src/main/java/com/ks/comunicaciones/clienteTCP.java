package com.ks.comunicaciones;

import com.ks.tcp.Cliente;
import com.ks.tcp.EventosTCP;
import com.ks.tcp.Tcp;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by migue on 16/10/2015.
 */
public class clienteTCP extends Cliente implements EventosTCP {

    private String VMstrFile;
    private BufferedReader VMioFile;
    private int VMintContador;

    public clienteTCP() {
        VMstrFile = "";
        VMintContador = 0;
        this.setEventos(this);
    }

    public void setFile(String file) {
        VMstrFile = file;
    }

    public void openFile() {
        try {
            VMioFile = new BufferedReader(new FileReader(VMstrFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    public void conexionEstablecida(Cliente cliente) {
        System.out.println("Se conecto con el cliente");
        String line;
        try {
            if ((line = VMioFile.readLine()) != null) {
                System.out.println("Mensaje enviado 1");
                System.out.println(line);
                this.enviar(line);
                VMintContador += 1;
            }
        } catch (Exception ex) {
            System.out.println("Problema al mandar la transaccion: " + ex.getMessage());
        }
    }

    public void errorConexion(String s) {
        System.out.println("Problema al conectarse con el cliente: " + s);
    }

    public void datosRecibidos(String s, byte[] bytes, Tcp tcp) {
        System.out.println("Mensaje recibido " + VMintContador);
        System.out.println(s);
        String line;
        try {
            if ((line = VMioFile.readLine()) != null) {
                VMintContador += 1;
                System.out.println("Mensaje enviado " + VMintContador);
                System.out.println(line);
                this.enviar(line);
            } else {
                this.cerrar();
            }
        } catch (Exception ex) {
            System.out.println("Problema al mandar la transaccion: " + ex.getMessage());
        }
    }

    public void cerrarConexion(Cliente cliente) {

    }
}
